#!/usr/bin/env bash

pandoc --css "./static/tacit-css.min.css" --to=html5 -s -f markdown+smart+emoji+definition_lists --toc --toc-depth=2 --metadata pagetitle="Brendan Brown" resume.md -o index.html

sed -n "/<article>/,/<\/article>/p" resume.md | pandoc --to=pdf -s -f markdown+smart+emoji+definition_lists --pdf-engine=pdflatex -o ./static/bbrown_cv.pdf
