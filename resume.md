---
header-includes:
   <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
---

<section>
<article>
<figure>
<img src="static/avatar.png">
</figure>

## Experience

### LexiFi

#### Software Engineer 

*Sept. 2024 - present*

### Input Output Global

#### Software Engineer (Haskell)

*Sept. 2023 - July 2024*

Team member in a blockchain indexing framework project, and engineer in the innovation group, turning research into software design into proof-of-concept applications. IOG is the lead research and development organization behind the Cardano blockchain.

### NoviSci (Target RWE)

#### Advanced Statistical Software Developer

*May 2023 - Sept. 2023*

Primary Haskell developer for internal framework for processing electronic
health records data into analysis-ready datasets for statistical inference studies.
Leading R developer for licensed statistical libraries.

#### Senior Statistician for Software Development

*June 2021 - April 2023*

Software development in Haskell and R for statistical analysis, causal inference and large-scale
data-processing for in-house epidemiological studies using real-world
data.

### Dept. of Statistics and Operations Research, UNC Chapel Hill

#### Research assistant

*2019-2021*

Uncertainty quantification, multifidelity modeling and exploratory statistical
analysis of extreme ship motions and loads resulting from a computational
physics model, in grant funded by the Office of Naval Research.

#### Data science course development

*2017*

Core member for technical content of a group creating the first undergraduate
data science course in the department, part of the Data@Carolina initiative.
Created at least 30 percent of materials. A collaboration with S. Bhamidi, I.
Carmichael and D.  Glotzer.

#### Teaching

*2017-2021*

Walter L. Deemer Excellence in Teaching Award.

Instructor for Introduction to data science (STOR 320); Decision models for
business (STOR 113); Introduction to data models (STOR 155); Linear algebra
camp for 1st-year graduate students.

Assistant for Measure theory (STOR 634); Machine learning (STOR 565);
Introduction to data models (STOR 155); Real analysis camp for 1st-year
graduate students.

### General Administration, UNC System

#### Analyst, Finance Division

*2014-2016*

Forecasts and risk analyses of enrollment and financial aid requirements for
the finance division of North Carolina's 16-campus state university system.
Development of methods using a variety of machine learning tools, including
clustering, regression, tree methods, time series analysis, MCMC/Hamiltonian
Monte Carlo.

### NC Department of Stat Treasurer

#### Policy Analyst (contractor)

*2013-2014*

Data cleanup and statistical survival analysis of Teachers' and State
Employees' Retirement System datasets. Policy briefs for legislative or
executive audiences, including research and data analysis, written for
decision-makers.

## Academics

| Degree | Institution | Year |
|--------|-------------|------|
| PhD, Statistics | University of North Carolina at Chapel Hill | 2021 |
| Master of Public Policy | Duke University | 2014 |

### Research

* Convergence of reflected diffusions arising from interacting particle systems
* Monte carlo and coupling methods for statistical estimation of extremes for stochastic processes
* Master's project: Survival analysis of work in retirement for the N.C. Teachers' and State Employees' Retirement System

### Publications

**Note:** In the field of probability theory, authors are listed in order of
last name not of contribution.

[Inert drift system in a viscous fluid: Steady state asymptotics and exponential ergodicity](https://arxiv.org/abs/1905.11868)
~ with Sayan Banerjee
~ Trans. Amer. Math. Soc. 373 (2020), 6369-6409

[Dimension-free local convergence and perturbations for reflected Brownian motions](http://arxiv.org/abs/2009.12937)
~ with Sayan Banerjee
~ Ann. Appl. Probab. 33(1): 376-416 (February 2023)

[Convergence of reflected diffusions from interacting particles](https://cdr.lib.unc.edu/concern/dissertations/wm117x251)
~ Dissertation (2021)

On Extending Multifidelity Uncertainty Quantification from Non-Rare to Rare Problems
~ with Vladas Pipiras
~ Proceedings of the 17th International Ship Stability Workshop (2019)

## Technical skills

|Language | Proficiency |
|---------|-------|
| Haskell | $\star \star \star \star$ |
| R | $\star \star \star \star$ |
| SQL | $\star\star$ |
| Rust | $\star \star$ |
| Python |$\star \star$ |
| Bash | $\star \star$ |
| Stan | $\star$ |

Linux user for more than 10 years at home and at work. Moderate experience with deployment and CI environments using Docker and Nix.

From $\star$, can-make-it-work, to $\star \star \star \star$, proficient and professional.

## Projects

### [random-cycle](https://hackage.haskell.org/package/random-cycle)
A Haskell library for efficient uniform random sampling of cycle partition
graphs on sets of vertices, and partitions of lists or vectors. Selection can
be subject to conditions. A personal project.

### [interval-algebra](https://hackage.haskell.org/package/interval-algebra)

An implementation of [Allen's interval
algebra](https://en.wikipedia.org/wiki/Allen%27s_interval_algebra) for temporal
logic, used regularly for epidemiological applications at NoviSci (Target RWE).
Credit for a substantial portion of the code goes to the previous author, but I
maintained the package for most 2023 and am responsible for most of the
code in the `Core` module.


</article>

<footer>
<nav>
<ul>
<li>[:page_facing_up:](static/bbrown_cv.pdf)</li>
<li>[:key:](brendanbrown.gpg)</li>
<li><a href="https://gitlab.com/brendanrbrown"><img src="static/git.svg"></a></li>
<li><a href="https://balaena.digital">:whale:</a></li>
</ul>
</nav>
<nav>
Made with [pandoc](https://pandoc.org/) and [tacit, a minimal css](https://github.com/yegor256/tacit).
</nav>
</footer>
</section>
